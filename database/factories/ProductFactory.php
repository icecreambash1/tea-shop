<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title'=>$this->faker->title(20),
            'description'=>$this->faker->realText(50),
            'price'=>$this->faker->numberBetween(500,900),
            'count'=>$this->faker->numberBetween(20,40),
        ];
    }
}
