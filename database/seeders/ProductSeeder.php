<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            'Пуэр зеленый',
            'Пуэр черный',
            'Красный каркаде',
            'Зеленый кофе',
            'Зеленый чай',
            'Красная роза',
            'Улун белый',
            'Липтен',
            'Что-то ещё',
            'Какой-то ещё чай',
            'Что-то не понятное'
        ];

        foreach ($products as $product) {
            Product::factory()->create(
                    [
                        'title'=>$product,
                        'category_id'=>Category::all()->random()->id
                    ]
            );
        }
    }
}
