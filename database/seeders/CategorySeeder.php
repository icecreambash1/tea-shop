<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::factory()->count(2)->create();

        $category_parents = [
            'Мята',
            'Пуэр',
            'Каркаде',
            'Луда'
        ];


        foreach ($category_parents as $category_parent) {
            Category::create(
                [
                    'title'=>$category_parent,
                    'parent_id'=>Category::all()->where('parent_id',null)->random()->id
                ]
            );
        }
    }
}
