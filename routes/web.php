<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;







Route::resource('category', CategoryController::class)->only(
    [
        'index',
        'show',
    ]
);
Route::resource('product', ProductController::class)->only(
    [
        'show',
    ]
);

Route::resource('cart', CartController::class,)
    ->only(
        [
           'index',
        ]
    )->middleware('auth');

Route::group(['prefix'=>'cart','middleware'=>['auth']], function (){
    Route::get('/addProduct/{id}',[CartController::class,'addProduct'])->name('add_product_to_cart');
    Route::get('/destroyProduct/{id}',[CartController::class,'deleteProduct'])->name('delete_product_to_cart'); // Заменить на delete, нужно было установить ajax
});


Route::group(['prefix'=>'auth'], function (){
    Route::post('/login',[AuthController::class,'login'])->name('login');
    Route::get('/login',[AuthController::class,'loginShow']);
    Route::get('/logout',[AuthController::class,'logOut'])->name('logout');
});



Route::get('/',function (){
    return redirect()->route('category.index');
})->name('dashboard');
