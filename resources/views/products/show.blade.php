<body>
@include('header')
<div style="text-align: center">
    <h1>{{$product->title}}</h1>
    <h2>{{$product->description}}</h2>
    <h3>В наличие: {{$product->count}}</h3>
    <h3>Цена: {{$product->price}} $</h3>
    <a href="{{route('add_product_to_cart',$product->id)}}"><button>Добавить в корзину</button></a>
</div>
</body>
