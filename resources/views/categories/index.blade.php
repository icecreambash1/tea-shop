
<body>
@include('header')
<h2>Категории</h2>
@foreach($categories as $category)
    <div>
        <a href="{{route('category.show',$category->id)}}">{{$category->title}}</a>

        @foreach($category->children as $child)
            <div style="margin-left: 20px">
                <a href="{{route('category.show',$child->id)}}">{{$child->title}}</a>
            </div>
        @endforeach
    </div>
@endforeach
</body>
