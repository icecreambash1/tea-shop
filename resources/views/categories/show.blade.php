<body>
@include('header')
<h2 style="text-align: center"><a href="{{url()->previous()}}">Назад</a> Категория {{$category->title}}</h2>
<div style="text-align: center">
<ul>
@foreach($category->products as $product)
        <li><a href="{{route('product.show',$product->id)}}">{{$product->title}}</a></li>
@endforeach
</ul>
</div>
</body>
