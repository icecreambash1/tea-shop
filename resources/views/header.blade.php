@if(\Illuminate\Support\Facades\Auth::check())
    <h4>Hi, {{auth()->user()->name}}({{auth()->user()->email}}) <a href="{{route('logout')}}"/>Выйти</a></h4>
    <h4><a href="{{route('category.index')}}">Категории</a> <a href="{{route('cart.index')}}">Корзина</a></h4>
@else
    <div><a href="{{route('login')}}">Войти</a></div>
@endif
