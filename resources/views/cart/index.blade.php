
<style>
    table, th, td {
        border: 1px solid;
    }

    table {
        width: 100%;
    }
</style>
<body>
@include('header')
<h2 style="text-align: center">Корзина | Сумма: {{$totalprice}} $</h2>

<div style="margin: auto; width: 50%">
    <table>
        <tr>
            <th>Название продукта</th>
            <th>Кол-во продукта в корзине</th>
            <th>Инструменты</th>
        </tr>
        @foreach($products as $product)
            <tr>
                <th><a href="{{route('product.show',$product->products()->first()->id)}}">{{$product->products()->first()->title}}</a></th>
                <th>{{$product->count}}</th>
                <th>
                    <a href="{{route('add_product_to_cart',$product->product_id)}}"><button>Добавить</button></a>
                    <a href="{{route('delete_product_to_cart',$product->product_id)}}"><button>Удалить</button></a>
                </th>
            </tr>
        @endforeach
    </table>
</div>
</body>

