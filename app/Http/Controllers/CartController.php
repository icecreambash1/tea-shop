<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartAddProductRequest;
use App\Http\Requests\CartDeleteProductRequest;
use App\Http\Requests\CartStoreRequest;
use App\Services\CartService;
use Illuminate\Http\Request;

class CartController extends Controller
{

    private object $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function index(Request $request)
    {
        return $this->cartService->showCart();
    }

    public function store(CartStoreRequest $request)
    {

    }

    public function addProduct(CartAddProductRequest $request)
    {
        return $this->cartService->addProductToCart($request->product_id);
    }

    public function deleteProduct(CartDeleteProductRequest $request)
    {
        return $this->cartService->deleteProductToCart($request->product_id);
    }
}
