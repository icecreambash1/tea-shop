<?php

namespace App\Http\Controllers;


use App\Http\Requests\CategoryShowRequest;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    private object $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index(): object
    {
        return $this->categoryService->getAllRootCategories();
    }

    public function show(CategoryShowRequest $request)
    {
        return $this->categoryService->getCategoryWithChildAndProducts($request->id);
    }
}
