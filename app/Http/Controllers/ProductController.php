<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductShowRequest;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    private object $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function show(ProductShowRequest $request)
    {
        return $this->productService->getProductById($request->id);
    }
}
