<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CartDeleteProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge(
            [
                'product_id'=>$this->route('id'),
            ]
        );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'product_id'=>'required|integer|exists:products,id'
        ];
    }
}
