<?php

namespace App\Services;

use App\Models\Category;

class CategoryService
{

    public function getAllRootCategories(): object
    {
       $categories = Category::root()->get();

       return view('categories.index',
       [
           'categories'=>$categories
       ]);
    }

    public function getCategoryWithChildAndProducts($id)
    {
        $category = Category::find($id);

        return view('categories.show',
        [
            'category'=>$category
        ]);
    }
}
