<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;

class AuthService
{
    public function login($payload)
    {
        if (Auth::attempt($payload)) {
            request()->session()->regenerate();
            return redirect()->route('category.index');
        }
    }
}
