<?php

namespace App\Services;

use App\Models\Product;

class ProductService
{
    public function getProductById($id)
    {
        $product = Product::find($id);

        return view('products.show',
        [
            'product'=>$product,
        ]);
    }
}
