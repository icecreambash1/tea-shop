<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Support\Facades\DB;

class CartService
{

    public function showCart()
    {
        $user = auth()->user();
        $cart = $user->cart()->first();
        $totalprice = 0;


        $cartItems = $cart->items()->orderBy('id','desc')->get();

        //Total Price
        $totalpriceArray = array_map(function ($item) use ($totalprice){
            $product = Product::find($item['product_id']);
            $totalprice =+ $product->price * $item['count'];
            return $totalprice;
        },$cartItems->toArray());

        foreach ($totalpriceArray as $item)
        {
            $totalprice += $item;
        }
        return view('cart.index',
        [
            'products'=>$cartItems,
            'totalprice'=>$totalprice
        ]);
    }

    public function addProductToCart($id)
    {
        $user = auth()->user();

        $cart = $user->cart()->first();
        $cartItems = $cart->items();

        if ($cartItems->where('product_id', $id)->exists()) {
            if (Product::where('id',$id)->first()->count === 0 or Product::where('id',$id)->first()->count === $cartItems->where('product_id',$id)->first()->count)
            {
                return redirect()->back();
            }
            $cartItems->first()->update(['count' => DB::raw('count+1')]);

            return redirect()->back();
        }

        $cartItems->create(
            [
                'product_id' => $id,
                'count' => 1,
            ]
        );
        return redirect()->back();

    }

    public function deleteProductToCart($id)
    {
        $user = auth()->user();

        $cart = $user->cart()->first();
        $cartItems = $cart->items();

        if ($cartItems->where('product_id', $id)->exists()) {
            if ($cartItems->first()->count === 1)
            {
                $cartItems->first()->delete();
                return redirect()->back();
            }

            $cartItems->first()->update(['count' => DB::raw('count-1')]);
            return redirect()->back();
        }
        return redirect()->back();

    }

}
