<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id',
        'user_id',
    ];

    public function items()
    {
        return $this->hasMany(CartProducts::class,'cart_id','id');
    }
}
