<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartProducts extends Model
{
    use HasFactory;

    protected $fillable = [
        'cart_id',
        'product_id',
        'count',
    ];

    public $timestamps = false;

    public function products()
    {
        return $this->hasOne(Product::class,'id','product_id');
    }
}
